# AWS
variable "aws_region" {
  type        = string
  description = "The AWS region where the environment will be deployed"
  default     = "eu-central-1"
}
# DNS
variable "dns_domain_name" {
  type        = string
  description = "The root domain name of the desired DNS"
  default     = "playground.johnfli.work"
}
# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment"
  default     = "test"
}
# VPC
variable "vpc_cidr" {
  type        = string
  description = "The CIDR block that will be used for the VPC"
  default     = "10.0.0.0/16"
}
# Website
variable "website_name" {
  description = "The name of the website that will be created"
  type        = string
  default     = "mysimplewebsite"
}

####################################################################################################################################
# IMPORTANT: (action needed)
# VARIABLES BELOW THIS POINT HAVE TO BE SETUP ON Infrastructure as Code management tool (e.g. CLI, Terraform Cloud, Spacelift, etc.)
####################################################################################################################################

# RDS
variable "rds_master_password" {
  type        = string
  description = "The master password that will be used for RDS (required to be set)"
}
