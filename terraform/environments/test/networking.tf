module "networking" {
  source = "../../modules/networking"

  providers = {
    aws  = aws
    null = null
  }

  env_name = var.env_name
  vpc_cidr = var.vpc_cidr
  # Enable the following for production-readiness
  vpc_flow_logs_bucket_arn        = ""
  vpc_enable_flow_logs_cloudwatch = false
}
