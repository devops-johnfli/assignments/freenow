module "dns" {
  source = "../../modules/dns"

  providers = {
    aws        = aws
  }

  env_name = var.env_name

  dns_domain_name = var.dns_domain_name
}

