module "rds" {
  source = "../../modules/rds"

  providers = {
    aws  = aws
    null = null
  }

  env_name = var.env_name

  vpc_id                          = module.networking.vpc_id
  vpc_database_subnet_group_name  = module.networking.database_subnet_group_name
  vpc_private_subnets_cidr_blocks = module.networking.private_subnets_cidr_blocks

  rds_enable_aurora_opt_storage_type = false
  rds_engine_version                 = "15.3"
  rds_master_password                = var.rds_master_password
  rds_instance_type                  = "db.r6g.large"

  # Automaticall create a database for the website upon RDS creation
  rds_database_name = var.website_name

  # Logging & Monitoring
  # Enable the following for production-readiness (disabled here for cost-reduction)
  rds_enable_cloudwatch_logs            = false
  dns_enable_enhanced_monitoring        = false
  rds_create_db_cluster_activity_stream = false
}

