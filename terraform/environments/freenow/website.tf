module "mysimplewebsite" {
  source = "../../modules/website"

  depends_on = [
    module.rds,
    module.s3
  ]

  providers = {
    aws = aws
  }
  # Website
  website_name = var.website_name
  # ENV
  env_name = var.env_name
  # VPC
  vpc_id                  = module.networking.vpc_id
  vpc_private_subnets_ids = module.networking.private_subnets
  vpc_public_subnets_ids  = module.networking.public_subnets
  # DNS
  dns_route53_zone_id = module.dns.hosted_zone_id
  dns_certificate_arn = module.dns.certificate_arn
  # ECS
  ecs_ec2_instance_type = "t2.micro"
  # Enable for production-readiness
  ecs_enable_logging_and_monitoring = false
  # Container
  ecs_container_name           = "${var.website_name}_nginx"
  ecs_container_port           = 80
  ecs_container_image_name     = "public.ecr.aws/nginx/nginx"
  ecs_container_image_tag      = "alpine"
  ecs_container_health_command = ["CMD-SHELL", "wget -O /dev/null http://localhost || exit 1"]
  # Autoscaling
  ecs_autoscaling_min          = 1
  ecs_autoscaling_max          = 5
  ecs_autoscaling_also_on_spot = true
  # Task
  ecs_task_cpu    = 700
  ecs_task_memory = 768
}

