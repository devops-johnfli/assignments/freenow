module "s3" {
  source = "../../modules/s3"

  providers = {
    aws = aws
  }

  env_name = var.env_name

  s3_enable_versioning = false
  s3_object_expiration_days = 0
}
