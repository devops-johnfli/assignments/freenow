################################################################################
# RDS Aurora Module
################################################################################

module "postgresql" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 9.0.0"

  name           = local.name
  engine         = "aurora-postgresql"
  engine_version = local.engine_version

  instance_class = local.instance_type

  master_username = "root"
  master_password = local.master_password
  # TODO: Set master password in a safer way for production-readiness
  # manage_master_user_password = true
  # master_user_secret_kms_key_id = var.kms_key_arn_or_key_id

  # Enable IAM authentication for passwordless access to RDS
  iam_database_authentication_enabled = true

  apply_immediately          = true
  auto_minor_version_upgrade = true

  # Use backup.tf for more fine-grained backup configuration
  # backup_retention_period      = 35
  # preferred_backup_window      = "03:00-05:00"
  # preferred_maintenance_window = "sun:05:00-sun:06:00"

  storage_encrypted = true
  storage_type      = local.storage_type


  vpc_id               = local.vpc_id
  db_subnet_group_name = local.vpc_database_subnet_group_name
  security_group_rules = {
    private_subnet_ingress = {
      cidr_blocks = local.vpc_private_subnets_cidr_blocks
      description = "Allow access from VPC private subnets"
    }
  }

  # TODO: Comment out the following for production
  skip_final_snapshot = true

  # Keep the DB Cluster Parameter Group name same as the DB Parameter Group name for the sake of simplicity 
  create_db_cluster_parameter_group      = true
  db_cluster_parameter_group_name        = local.db_parameter_group_name
  db_cluster_parameter_group_family      = "aurora-postgresql${split(".", local.engine_version)[0]}"
  db_cluster_parameter_group_description = "${local.db_parameter_group_name} cluster parameter group"
  db_cluster_parameter_group_parameters = [
    {
      name         = "log_min_duration_statement"
      value        = 4000
      apply_method = "immediate"
    },
    {
      name         = "rds.force_ssl"
      value        = 1
      apply_method = "immediate"
    }
  ]

  create_db_parameter_group      = true
  db_parameter_group_name        = local.db_parameter_group_name
  db_parameter_group_family      = "aurora-postgresql${split(".", local.engine_version)[0]}"
  db_parameter_group_description = "${local.db_parameter_group_name} DB parameter group"
  db_parameter_group_parameters = [
    {
      name         = "log_min_duration_statement"
      value        = 4000
      apply_method = "immediate"
    }
  ]

  # Automatically create a database by the following name on cluster creation
  database_name = local.database_name != "" ? local.database_name : null

  # CloudWatch
  create_cloudwatch_log_group     = local.enable_cloudwatch_logs
  enabled_cloudwatch_logs_exports = local.enable_cloudwatch_logs ? ["audit", "error", "postgresql"] : []

  # Enhanced monitoring
  monitoring_interval    = local.enable_enhanced_monitoring ? 10 : 0
  create_monitoring_role = local.enable_enhanced_monitoring

  # Cluster Activity Stream
  create_db_cluster_activity_stream = local.create_db_cluster_activity_stream
  # Source: https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/DBActivityStreams.Overview.html
  # TODO: Use KMS key for production-readiness
  # db_cluster_activity_stream_kms_key_id = module.kms.key_id

  tags = local.tags
}

################################################################################
# Supporting Resources
################################################################################

module "kms" {
  # TODO: Conditional creation
  source  = "terraform-aws-modules/kms/aws"
  version = "~> 2.0"

  deletion_window_in_days = 7
  description             = "KMS key for ${local.name} cluster activity stream."
  enable_key_rotation     = true
  is_enabled              = true
  key_usage               = "ENCRYPT_DECRYPT"

  aliases = [local.name]

  tags = local.tags
}
