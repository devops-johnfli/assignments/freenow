# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment this RDS cluster will be part of"
  default     = "test"
}
# VPC
variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which the RDS will be created (required)"
}
variable "vpc_database_subnet_group_name" {
  type        = string
  description = "The name of the VPC subnet group name that will be assigned to RDS (required)"
}
variable "vpc_private_subnets_cidr_blocks" {
  type        = list(string)
  description = "The CIDR blocks of the VPC private subnets that will be assigned access to the RDS (required)"
}
# RDS
variable "rds_additional_tags" {
  description = "Custom additional tags for the RDS cluster"
  type        = map(string)
  default     = {}
}
variable "rds_enable_aurora_opt_storage_type" {
  type        = bool
  description = "Enable RDS Aurora optimized storage type provisioning"
  default     = false
}
variable "rds_engine_version" {
  type        = string
  description = "The version of the PostgreSQL version"
  default     = "15.3"
}
variable "rds_database_name" {
  type        = string
  description = "Name for an automatically created database on cluster creation"
  default     = ""
}
variable "rds_master_password" {
  type        = string
  description = "The master password that will be used for RDS"
  default     = "password"
}
variable "rds_instance_type" {
  type        = string
  description = "The instance type that will be provisioned for RDS"
  default     = "db.r6g.large"
}
# # RDS Logging & Monitoring
variable "rds_enable_cloudwatch_logs" {
  type        = bool
  description = "Enable CloudWatch log exports for RDS, for enhanced security and compliance"
  default     = false
}
variable "dns_enable_enhanced_monitoring" {
  type        = bool
  description = "Enable Enhanced Monitoring for RDS"
  default     = false
}
variable "rds_create_db_cluster_activity_stream" {
  type        = bool
  description = "Create DB cluster activity stream for enhanced security and compliance"
  default     = false
}
# Source: https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/DBActivityStreams.Overview.html
