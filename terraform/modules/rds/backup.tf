resource "aws_kms_key" "backup_key" {
  description = "KMS key for AWS Backup"
}

resource "aws_backup_vault" "rds_backups" {
  name        = "RDS_backup_vault"
  kms_key_arn = aws_kms_key.backup_key.arn
}

# Monthly backup plan:
# --------------------
# Keep one backup for the last 6 months,
# archiving all but for the most recent one
resource "aws_backup_plan" "monthly" {
  name = "monthly_backup_plan"

  rule {
    rule_name         = "monthly_backup_rule"
    target_vault_name = aws_backup_vault.rds_backups.name
    # Cron format            = "cron(m h DoM M DoW Y)"
    # where: m = minute, h = hour (UTC time), DoM = Day of Month, M = Month, DoW = Day of Week, Y = Year
    schedule                 = "cron(0 7 1 * ?)"
    enable_continuous_backup = false

    lifecycle {
      cold_storage_after = 31
      delete_after       = 186
      # 6 months = 6 * 31 days = 186
    }
  }
}


# Weekly backup plan:
# ------------------
# Keep one backup for the last 3 weeks
resource "aws_backup_plan" "weekly" {
  name = "weekly_backup_plan"

  rule {
    rule_name         = "weekly_backup_rule"
    target_vault_name = aws_backup_vault.rds_backups.name
    # Cron format            = "cron(m h DoM M DoW Y)"
    # where: m = minute, h = hour (UTC time), DoM = Day of Month, M = Month, DoW = Day of Week, Y = Year
    schedule                 = "cron(0 0 ? * 1 *)"
    enable_continuous_backup = false

    lifecycle {
      cold_storage_after = 0
      delete_after       = 21
      # 3 weeks = 3 * 7 days = 21
    }

  }
}
# Source: https://docs.rubrik.com/en-us/saas/saas/aws_rds_continuous_backups.html#:~:text=Amazon%20RDS%20continuous%20backups,(PITR)%2C%20or%20both.

# Daily backup plan:
# ------------------
# Keep continuous backups for the last 7 days
resource "aws_backup_plan" "daily" {
  name = "daily_backup_plan"

  rule {
    rule_name         = "daily_backup_rule"
    target_vault_name = aws_backup_vault.rds_backups.name
    # Cron format            = "cron(m h DoM M DoW Y)"
    # where: m = minute, h = hour (UTC time), DoM = Day of Month, M = Month, DoW = Day of Week, Y = Year
    schedule                 = "cron(0 3 ? * * *)"
    enable_continuous_backup = true

    lifecycle {
      cold_storage_after = 0
      delete_after       = 7
    }

  }
}
# Source: https://docs.rubrik.com/en-us/saas/saas/aws_rds_continuous_backups.html#:~:text=Amazon%20RDS%20continuous%20backups,(PITR)%2C%20or%20both.

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}
resource "aws_iam_role" "backup_role" {
  name               = "AWS_backup_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "backup_role_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_role.name
}

resource "aws_backup_selection" "rds_backup_monthly" {
  iam_role_arn = aws_iam_role.backup_role.arn
  name         = "RDS_monthly_backup"
  plan_id      = aws_backup_plan.monthly.id

  resources = [
    module.postgresql.cluster_arn
  ]
}

resource "aws_backup_selection" "rds_backup_weekly" {
  iam_role_arn = aws_iam_role.backup_role.arn
  name         = "RDS_weekly_backup"
  plan_id      = aws_backup_plan.weekly.id

  resources = [
    module.postgresql.cluster_arn
  ]
}

resource "aws_backup_selection" "rds_daily_backup" {
  iam_role_arn = aws_iam_role.backup_role.arn
  name         = "RDS_daily_backup"
  plan_id      = aws_backup_plan.daily.id

  resources = [
    module.postgresql.cluster_arn
  ]
}
# Source: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_selection.html
