locals {
  name = var.env_name

  master_password = var.rds_master_password

  db_parameter_group_name = local.name
  engine_version          = var.rds_engine_version
  instance_type           = var.rds_instance_type
  storage_type            = var.rds_enable_aurora_opt_storage_type ? "aurora-iopt1" : ""

  enable_enhanced_monitoring        = var.dns_enable_enhanced_monitoring
  enable_cloudwatch_logs            = var.rds_enable_cloudwatch_logs
  create_db_cluster_activity_stream = var.rds_create_db_cluster_activity_stream

  database_name = var.rds_database_name

  tags = merge({
    environment = var.env_name
  }, var.rds_additional_tags)

  vpc_id                          = var.vpc_id
  vpc_database_subnet_group_name  = var.vpc_database_subnet_group_name
  vpc_private_subnets_cidr_blocks = var.vpc_private_subnets_cidr_blocks
}
