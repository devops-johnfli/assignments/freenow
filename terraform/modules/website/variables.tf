# Website
variable "website_name" {
  description = "The name of the website that will be created"
  type        = string
  default     = "mysimplewebsite"
}
variable "website_additional_tags" {
  description = "Custom additional tags for the RDS cluster"
  type        = map(string)
  default     = {}
}
# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment this S3 bucket will be part of"
  default     = "test"
}
# VPC
variable "vpc_id" {
  type        = string
  description = "The ID of the VPC in which the RDS will be created"
}
variable "vpc_private_subnets_ids" {
  type        = list(string)
  description = "The IDs of the VPC private subnets where the ECS resources will be deployed"
}
variable "vpc_public_subnets_ids" {
  type        = list(string)
  description = "The IDs of the VPC private subnets where the ECS resources will be deployed"
}
# DNS
variable "dns_route53_zone_id" {
  description = "The Route53 Hosted Zone ID, to be referenced by zone records that will be created by the Application Load Balancer (ALB) (required)"
  type        = string
}
variable "dns_certificate_arn" {
  description = "The ARN of the certificate that will be used by the Application Load Balancer (ALB) (required)"
  type        = string
}
# ECS
variable "ecs_ec2_instance_type" {
  description = "The instance type that will be used for the autoscaling group allocated to the ECS cluster"
  type        = string
  default     = "t2.micro"
}
variable "ecs_enable_logging_and_monitoring" {
  description = "Whether to enable CloudWatch logging and Container Insights for the ECS cluster"
  type        = bool
  default     = false
}
# # ECS Container
variable "ecs_container_name" {
  description = "The name of the ECS container that will be created"
  type        = string
  default     = "website_nginx"
}
variable "ecs_container_port" {
  description = "The port number of the ECS container that will be created"
  type        = number
  default     = 80
}
variable "ecs_container_image_name" {
  description = "The image of the ECS container that will be created"
  type        = string
  default     = "public.ecr.aws/nginx/nginx"
}
variable "ecs_container_image_tag" {
  description = "The image tag of the ECS container that will be created"
  type        = string
  default     = "latest"
}
variable "ecs_container_health_command" {
  description = "The health check command for the ECS container that will be created, based on the depoyed image"
  type        = list(string)
  default     = ["CMD", "service", "nginx", "status"]
}
# # ECS Autoscaling
variable "ecs_autoscaling_min" {
  description = "The minimum number of ECS task replicas that will be running"
  type        = number
  default     = 1
}
variable "ecs_autoscaling_max" {
  description = "The maximum number of ECS task replicas that will be running"
  type        = number
  default     = 1
}
variable "ecs_autoscaling_also_on_spot" {
  description = "Whether to run ECS task replicas on spot instances as well"
  type        = bool
  default     = true
}
# # ECS Task
variable "ecs_task_cpu" {
  description = "The image tag of the ECS container that will be created"
  type        = number
  default     = 700
}
variable "ecs_task_memory" {
  description = "The image tag of the ECS container that will be created"
  type        = number
  default     = 768
}
