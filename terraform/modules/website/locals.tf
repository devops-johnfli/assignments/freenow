locals {
  name = var.website_name != "" ? var.website_name : var.env_name

  # Task
  task_cpu    = var.ecs_task_cpu
  task_memory = var.ecs_task_memory

  # Container
  container_name           = var.ecs_container_name
  container_port           = var.ecs_container_port
  container_image_name     = var.ecs_container_image_name
  container_image_tag      = var.ecs_container_image_tag
  container_health_command = var.ecs_container_health_command

  # Autoscaling
  autoscaling_min          = var.ecs_autoscaling_min
  autoscaling_max          = var.ecs_autoscaling_max
  autoscaling_also_on_spot = var.ecs_autoscaling_also_on_spot

  ec2_instance_type = var.ecs_ec2_instance_type

  # Logging
  enable_logging_and_monitoring = var.ecs_enable_logging_and_monitoring

  route53_zone_id = var.dns_route53_zone_id
  certificate_arn = var.dns_certificate_arn

  tags = merge({
    environment = var.env_name
    website     = local.name
  }, var.website_additional_tags)

  # VPC
  vpc_id                  = var.vpc_id
  vpc_public_subnets_ids  = var.vpc_public_subnets_ids
  vpc_private_subnets_ids = var.vpc_private_subnets_ids
}
