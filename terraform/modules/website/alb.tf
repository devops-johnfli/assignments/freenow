# Source: https://github.com/terraform-aws-modules/terraform-aws-ecs/tree/v5.7.4/websiteamples/complete
# Source: https://spacelift.io/blog/terraform-ecs

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 9.0"

  name = local.name

  load_balancer_type = "application"

  vpc_id  = local.vpc_id
  subnets = local.vpc_public_subnets_ids

  # Set to true for production-readiness
  enable_deletion_protection = false

  # Security Group
  security_group_ingress_rules = {
    all_http = {
      from_port   = 80
      to_port     = 80
      ip_protocol = "tcp"
      description = "Allow HTTP web traffic"
      cidr_ipv4   = "0.0.0.0/0"
    }
    all_https = {
      from_port   = 443
      to_port     = 443
      ip_protocol = "tcp"
      description = "Allow HTTPS web traffic"
      cidr_ipv4   = "0.0.0.0/0"
    }
  }
  security_group_egress_rules = {
    all = {
      ip_protocol = "-1"
      description = "Allow all egress traffic"
      cidr_ipv4   = "0.0.0.0/0"
    }
  }

  listeners = {
    website_http = {
      port     = 80
      protocol = "HTTP"

      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
    website_https = {
      port            = 443
      protocol        = "HTTPS"
      certificate_arn = local.certificate_arn

      forward = {
        target_group_key = "website_ecs"
      }
    }
  }

  target_groups = {
    website_ecs = {
      backend_protocol                  = "HTTP"
      backend_port                      = local.container_port
      target_type                       = "ip"
      deregistration_delay              = 5
      load_balancing_cross_zone_enabled = true

      health_check = {
        enabled             = true
        healthy_threshold   = 5
        interval            = 30
        matcher             = "200"
        path                = "/"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = 5
        unhealthy_threshold = 2
      }

      # Theres nothing to attach here in this definition. Instead,
      # ECS will attach the IPs of the tasks to this target group
      create_attachment = false
    }
  }

  route53_records = {
    website = {
      zone_id = local.route53_zone_id
      name    = local.name
      type    = "A"

      tags = local.tags
    }
  }

  # TODO: Enhance security by enabling WAF on ALB & passing WAF ARN
  associate_web_acl = false
  # web_acl_arn = var.waf_arn

  tags = local.tags
}
