################################################################################
# ECS Cluster
################################################################################

module "ecs_cluster" {
  source  = "terraform-aws-modules/ecs/aws//modules/cluster"
  version = "~> 5.7.4"

  cluster_name = local.name

  # Capacity provider - autoscaling groups
  default_capacity_provider_use_fargate = false
  autoscaling_capacity_providers = {
    # On-demand instances
    cp_on_demand = {
      auto_scaling_group_arn         = module.autoscaling["asg-on-demand"].autoscaling_group_arn
      managed_termination_protection = "ENABLED"

      managed_scaling = {
        instance_warmup_period    = 300
        maximum_scaling_step_size = 3
        minimum_scaling_step_size = 1
        status                    = "ENABLED"
        target_capacity           = 60
      }

      default_capacity_provider_strategy = {
        weight = 60
        base   = 20
      }
    }
    # Spot instances
    cp_spot = {
      auto_scaling_group_arn         = module.autoscaling["asg-spot"].autoscaling_group_arn
      managed_termination_protection = "ENABLED"

      managed_scaling = {
        instance_warmup_period    = 300
        maximum_scaling_step_size = 15
        minimum_scaling_step_size = 5
        status                    = "ENABLED"
        target_capacity           = 90
      }

      default_capacity_provider_strategy = {
        weight = 40
      }
    }
  }


  cluster_settings = local.enable_logging_and_monitoring ? { "name" : "containerInsights", "value" : "enabled" } : { "name" : "containerInsights", "value" : "disabled" }

  tags = local.tags
}
# Source: https://github.com/terraform-aws-modules/terraform-aws-ecs/tree/v5.7.4/examples/ec2-autoscaling
