################################################################################
# ECS Service / Task / Container
################################################################################

module "ecs_service" {
  source  = "terraform-aws-modules/ecs/aws//modules/service"
  version = "~> 5.7.4"

  # Service
  name        = local.name
  cluster_arn = module.ecs_cluster.arn

  requires_compatibilities = ["EC2"]


  # Task Definition
  create_task_definition = true

  desired_count = local.autoscaling_min

  cpu    = local.task_cpu
  memory = local.task_memory
  capacity_provider_strategy = {
    # On-demand instances
    cps_on_demand = {
      capacity_provider = module.ecs_cluster.autoscaling_capacity_providers["cp_on_demand"].name
      weight            = 50
      base              = 1
    }
    cps_spot = {
      capacity_provider = module.ecs_cluster.autoscaling_capacity_providers["cp_spot"].name
      weight            = 50
    }
  }

  volume = {
    website = {}
  }

  task_tags = local.tags

  create_tasks_iam_role = true
  tasks_iam_role_policies = {
    AmazonECSTaskExecutionRolePolicy = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  }

  # Container definition(s)
  container_definitions = {
    (local.container_name) = {
      image = "${local.container_image_name}:${local.container_image_tag}"

      cpu = local.task_cpu
      # Source: https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html

      port_mappings = [
        {
          name          = local.container_name
          hostPort      = local.container_port
          containerPort = local.container_port
          protocol      = "tcp"
        }
      ]
      # Source: https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_PortMapping.html

      mount_points = [
        {
          sourceVolume  = "website",
          containerPath = "/var/www/website"
        }
      ]

      health_check = {
        command = local.container_health_command
        # Source: https://stackoverflow.com/questions/48776044/docker-healthcheck-for-nginx-container
      }
      # Source: https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_HealthCheck.html

      readonly_root_filesystem = false

      enable_cloudwatch_logging = local.enable_logging_and_monitoring

      # Autoscaling
      enable_autoscaling       = true
      autoscaling_min_capacity = local.autoscaling_min
      autoscaling_max_capacity = local.autoscaling_max
      autoscaling_policies = {
        cpu = {
          policy_type = "TargetTrackingScaling"

          target_tracking_scaling_policy_configuration = {
            predefined_metric_specification = {
              predefined_metric_type = "ECSServiceAverageCPUUtilization"
            }
          }
        }
        memory = {
          policy_type = "TargetTrackingScaling"

          target_tracking_scaling_policy_configuration = {
            predefined_metric_specification = {
              predefined_metric_type = "ECSServiceAverageMemoryUtilization"
            }
          }
        }
      }
    }
  }

  health_check_grace_period_seconds = 300

  load_balancer = {
    service = {
      target_group_arn = module.alb.target_groups["website_ecs"].arn
      container_name   = local.container_name
      container_port   = local.container_port
    }
  }

  subnet_ids = local.vpc_private_subnets_ids

  create_security_group = true
  security_group_rules = {
    alb_http_ingress = {
      type                     = "ingress"
      from_port                = local.container_port
      to_port                  = local.container_port
      protocol                 = "tcp"
      description              = "Allow HTTP ingress traffic from ${local.name} ALB"
      source_security_group_id = module.alb.security_group_id
    }
    all_egress = {
      type        = "egress"
      from_port   = "-1"
      to_port     = "-1"
      protocol    = "-1"
      description = "Allow all egress traffic"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  tags = local.tags
}
