locals {
  env_name = var.env_name

  domain_name = var.dns_domain_name

  subject_alternative_names = length(var.dns_subject_alternative_names) == 0 ? [
    "*.${local.domain_name}",
  ] : var.dns_subject_alternative_names

  distinct_domain_names = distinct(
    [for s in concat([local.domain_name], local.subject_alternative_names) : replace(s, "*.", "")]
  )

  # Get the list of distinct domain_validation_options, 
  # with wildcard domain names replaced by the domain name
  validation_domains = distinct(
    [
      for k, v in aws_acm_certificate.cert.domain_validation_options :
      merge(tomap(v), { domain_name = replace(v.domain_name, "*.", "") })
    ]
  )

  tags = merge({
    environment = local.env_name
  }, var.dns_additional_tags)
}
