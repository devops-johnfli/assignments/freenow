# Create Route53 hosted zone for the provided DNS
resource "aws_route53_zone" "domain" {
  name    = local.domain_name
  comment = "This Route53 Hosted Zone was created in part of environment ${local.env_name} (created and managed by Terraform)"

  tags = local.tags
}
# Create an ACM Certificate
resource "aws_acm_certificate" "cert" {
  domain_name = local.domain_name

  subject_alternative_names = local.subject_alternative_names

  validation_method = "DNS"

  tags = local.tags

  lifecycle {
    create_before_destroy = true
  }
}
# Create a record for the afore-created certificate
resource "aws_route53_record" "cert_dns" {
  count = length(local.distinct_domain_names)

  allow_overwrite = true

  zone_id = aws_route53_zone.domain.zone_id

  name = element(local.validation_domains, count.index)["resource_record_name"]
  records = [
    element(local.validation_domains, count.index)["resource_record_value"]
  ]
  type = element(local.validation_domains, count.index)["resource_record_type"]
  ttl  = 300
}
# Validate afore-created certificate
resource "aws_acm_certificate_validation" "cert_validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_dns : record.fqdn]
}
