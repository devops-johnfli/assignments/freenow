################################################################################
# DNS
################################################################################

output "dns" {
  description = "The DNS of the created hosted zone"
  value       = local.domain_name
}

################################################################################
# Hosted Zone
################################################################################

output "hosted_zone_arn" {
  description = "The Amazon Resource Name (ARN) of the Hosted Zone"
  value       = aws_route53_zone.domain.arn
}

output "hosted_zone_id" {
  description = "The Hosted Zone ID, this can be referenced by zone records"
  value       = aws_route53_zone.domain.zone_id
}

output "hosted_zone_name_servers" {
  description = "A list of name servers in associated (or default) delegation set"
  value       = aws_route53_zone.domain.name_servers
}

output "hosted_zone_primary_name_server" {
  description = "The Route 53 name server that created the SOA record."
  value       = aws_route53_zone.domain.primary_name_server
}

################################################################################
# Certificate
################################################################################

output "certificate_arn" {
  description = "ARN of the certificate"
  value       = aws_acm_certificate.cert.arn
}

output "certificate_id" {
  description = "ARN of the certificate"
  value       = aws_acm_certificate.cert.id
}

output "certificate_domain_name" {
  description = "Domain name for which the certificate is issued"
  value       = aws_acm_certificate.cert.domain_name
}

output "certificate_domain_validation_options" {
  description = "Set of domain validation objects which can be used to complete certificate validation"
  value       = aws_acm_certificate.cert.domain_validation_options
}

output "certificate_status" {
  description = "Status of the certificate"
  value       = aws_acm_certificate.cert.status
}

output "certificate_type" {
  description = "Source of the certificate"
  value       = aws_acm_certificate.cert.type
}

################################################################################
# Certificate DNS Record
################################################################################

output "records_cert_dns_names" {
  description = "The names of the DNS records for ACM certificate validation"
  value       = aws_route53_record.cert_dns[*].name
}

output "records_cert_dns_fqdn" {
  description = "The FQDNs (built using the zone domain and name) of the DNS records for ACM certificate validation"
  value       = aws_route53_record.cert_dns[*].fqdn
}

################################################################################
# Certificate Validation
################################################################################

output "cert_validation_id" {
  description = "Time at which the certificate was issued"
  value       = aws_acm_certificate_validation.cert_validation.id
}
