# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment these DNS resources will be part of"
  default     = "test"
}
# DNS
variable "dns_domain_name" {
  type        = string
  description = "The root domain name of the desired DNS"
  default     = "free-now.com"
}
variable "dns_subject_alternative_names" {
  description = "A list of domains that should be SANs in the issued certificate"
  type        = list(string)
  default     = []
}
variable "dns_additional_tags" {
  description = "Custom additional tags for the DNS resources"
  type        = map(string)
  default     = {}
}
