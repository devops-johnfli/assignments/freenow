data "aws_caller_identity" "current" {}

data "aws_region" "current" {}


data "aws_iam_policy_document" "s3_content_key" {
  policy_id = "unitary-S3-content-key-policy"
  statement {
    sid = "Enable IAM User Permissions"

    principals {
      type        = "AWS"
      identifiers = concat(["arn:aws:iam::${local.aws_account_id}:root"])
    }

    actions = [
      "kms:*",
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_kms_key" "s3_content_key" {
  description             = "This key is used to encrypt files in the S3 bucket ${local.name}"
  deletion_window_in_days = 7
  policy                  = data.aws_iam_policy_document.s3_content_key.json
  tags                    = local.tags
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = local.name
  tags   = local.tags
}

data "aws_iam_policy_document" "s3_bucket" {
  statement {
    principals {
      type        = "AWS"
      identifiers = concat(["arn:aws:iam::${local.aws_account_id}:root"])
    }

    actions = [
      "s3:*",
    ]

    resources = [
      aws_s3_bucket.s3_bucket.arn,
      "${aws_s3_bucket.s3_bucket.arn}/*",
    ]

  }
}

resource "aws_s3_bucket_policy" "s3_bucket" {
  bucket = aws_s3_bucket.s3_bucket.id
  policy = data.aws_iam_policy_document.s3_bucket.json
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3_bucket" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.s3_content_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_ownership_controls" "s3_bucket" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "s3_bucket" {
  count = local.object_expiration_days != 0 ? 1 : 0

  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    id     = "autodelete-media"
    status = "Enabled"

    expiration {
      days = local.object_expiration_days
    }
  }
}

resource "aws_s3_bucket_versioning" "s3_bucket" {
  count = local.enable_versioning ? 1 : 0

  bucket = aws_s3_bucket.s3_bucket.id

  versioning_configuration {
    status     = "Enabled"
    mfa_delete = "Disabled"
  }
}
