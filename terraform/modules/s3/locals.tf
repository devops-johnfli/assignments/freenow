locals {
  aws_account_id = data.aws_caller_identity.current.account_id
  aws_region     = data.aws_region.current.name

  name = var.s3_bucket_name != "" ? var.s3_bucket_name : "bucket.${local.aws_region}.${var.env_name}"

  enable_versioning = var.s3_enable_versioning

  object_expiration_days = var.s3_object_expiration_days
  tags = merge({
    environment = var.env_name
    name        = local.name
  }, var.s3_additional_tags)
}
