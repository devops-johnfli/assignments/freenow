# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment this S3 bucket will be part of"
  default     = "test"
}
# S3
variable "s3_bucket_name" {
  description = "The name of the S3 bucket that will be created"
  type        = string
  default     = ""
}
variable "s3_enable_versioning" {
  description = "Enable versioning on S3 bucket"
  type        = bool
  default     = false
}
variable "s3_object_expiration_days" {
  description = "The number of days after which the objects stored on the S3 bucket will expire and get deleted"
  type        = number
  default     = 0
}
variable "s3_additional_tags" {
  description = "Custom additional tags for the RDS cluster"
  type        = map(string)
  default     = {}
}