################################################################################
# VPC Endpoints
################################################################################
locals {
  endpoints = {
    s3 = {
      service             = "s3"
      private_dns_enabled = true

      security_group_ids = [
        aws_security_group.vpc_endpoints.id
      ]

      additional_tags = {
        name = "s3-vpc-endpoint"
      }

      policy = data.aws_iam_policy_document.s3_endpoint_policy.json
    },
    # TODO: Drop in case ECS tasks get stuck into provisioning state forever,
    # probably due to faulty configuration of this VPC endpoint
    ecs = {
      service             = "ecs"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets

      security_group_ids = [
        aws_security_group.vpc_endpoints.id
      ]

      additional_tags = {
        name = "ecs-vpc-endpoint"
      }
    },
    rds = {
      service             = "rds"
      private_dns_enabled = true
      subnet_ids          = module.vpc.database_subnets

      security_group_ids = [
        aws_security_group.rds_vpc_endpoint.id
      ]

      additional_tags = {
        name = "rds-vpc-endpoint"
      }
    },
  }
}

data "aws_vpc_endpoint_service" "this" {
  for_each = local.endpoints

  service = try(each.value.service, null)

  filter {
    name = "service-type"
    # Assuming default service type is Interface, unless stated otherwise 
    values = [try(each.value.service_type, "Interface")]
  }
}

resource "aws_vpc_endpoint" "this" {
  for_each = local.endpoints

  vpc_id       = module.vpc.vpc_id
  service_name = data.aws_vpc_endpoint_service.this[each.key].service_name
  # Assuming default service type is Interface, unless stated otherwise 
  vpc_endpoint_type = try(each.value.service_type, "Interface")
  auto_accept       = try(each.value.auto_accept, null)

  # Assuming default service type is Interface, unless stated otherwise 
  security_group_ids  = try(each.value.service_type, "Interface") == "Interface" ? length(distinct(lookup(each.value, "security_group_ids", []))) > 0 ? distinct(lookup(each.value, "security_group_ids", [])) : null : null
  subnet_ids          = try(each.value.service_type, "Interface") == "Interface" ? distinct(lookup(each.value, "subnet_ids", [])) : null
  route_table_ids     = try(each.value.service_type, "Interface") == "Gateway" ? lookup(each.value, "route_table_ids", null) : null
  policy              = try(each.value.policy, null)
  private_dns_enabled = try(each.value.service_type, "Interface") == "Interface" ? try(each.value.private_dns_enabled, null) : null

  tags = merge(local.tags, try(each.value.additional_tags, {}))
  dns_options {
    # Assuming configuration option for private DNS only for inbound resolver endpoint to be disabled, unless stated otherwise
    private_dns_only_for_inbound_resolver_endpoint = try(each.value.private_dns_only_for_inbound_resolver_endpoint, false)
  }
}
################################################################################
# Supporting Resources
################################################################################

# For S3 endpoint
data "aws_iam_policy_document" "s3_endpoint_policy" {
  statement {
    effect    = "Allow"
    actions   = ["*"]
    resources = ["*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "StringNotEquals"
      variable = "aws:SourceVpc"

      values = [module.vpc.vpc_id]
    }
  }
}
# For S3 and ECS endpoints
resource "aws_security_group" "vpc_endpoints" {
  name_prefix = "${local.name}-vpc-endpoints"
  description = "Firewalling for VPC endpoints inbound and outbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Allow all VPC-internal traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [module.vpc.vpc_cidr_block]
  }
  # TODO: Consider dropping this if not needed
  egress {
    description      = "Allow all egress traffic"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge({
    name = "vpc-endpoints"
  }, local.tags)

  lifecycle {
    create_before_destroy = true
  }
}
# For RDS endpoint
resource "aws_security_group" "rds_vpc_endpoint" {
  name_prefix = "${local.name}-rds-vpc-endpoint"
  description = "Firewalling for PostgreSQL VPC endpoint inbound and outbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = concat(module.vpc.private_subnets_cidr_blocks, module.vpc.database_subnets_cidr_blocks)
  }

  tags = merge({
    name = "rds-vpc-endpoint"
  }, local.tags)

  lifecycle {
    create_before_destroy = true
  }
}
