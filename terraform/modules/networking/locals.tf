locals {
  name = "${var.env_name}-vpc"

  vpc_cidr = var.vpc_cidr

  # Grab 3 AZs to meet both redundancy requirements, as well as compatibility with subnet CIDRs split below 
  azs = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = merge({
    environment = var.env_name
  }, var.vpc_additional_tags)

  vpc_flow_logs_bucket_arn        = var.vpc_flow_logs_bucket_arn
  vpc_enable_flow_logs_cloudwatch = var.vpc_enable_flow_logs_cloudwatch
}
