################################################################################
# VPC Module
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"


  name = local.name
  cidr = local.vpc_cidr

  azs              = local.azs
  private_subnets  = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 4, k)]
  public_subnets   = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 48)]
  database_subnets = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 52)]

  enable_dns_hostnames = true
  enable_dns_support   = true

  # Disable IPv6 addresses
  enable_ipv6 = false

  # Disable internet gateway, since we will use the public subnets to access the internet
  create_igw = true

  # Enable NAT gateway, since we will need internet access from the private subnets to access public ECR repositories
  enable_nat_gateway = true

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = local.vpc_enable_flow_logs_cloudwatch
  create_flow_log_cloudwatch_log_group = local.vpc_enable_flow_logs_cloudwatch
  create_flow_log_cloudwatch_iam_role  = local.vpc_enable_flow_logs_cloudwatch

  tags = local.tags
}

# VPC Flow Logs (S3 bucket)

resource "aws_flow_log" "vpc_flow_logs" {
  count                = local.vpc_flow_logs_bucket_arn != "" ? 1 : 0
  log_destination      = local.vpc_flow_logs_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = module.vpc.vpc_id
}

