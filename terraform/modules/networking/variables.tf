# ENV
variable "env_name" {
  type        = string
  description = "The name of the environment this VPC will be part of"
  default     = "test"
}
# VPC
variable "vpc_excluded_azs" {
  type        = list(string)
  description = "List of AZ names to be excluded for VPC"
  default     = []
}
variable "vpc_cidr" {
  type        = string
  description = "The CIDR block of the VPC"
  default     = "10.0.0.0/16"
}
variable "vpc_additional_tags" {
  description = "Custom additional tags for the VPC"
  type        = map(string)
  default     = {}
}
# # VPC Flow logs
variable "vpc_flow_logs_bucket_arn" {
  type        = string
  description = "ARN of the S3 bucket for VPC flow logs"
  default     = ""
}
variable "vpc_enable_flow_logs_cloudwatch" {
  type        = bool
  description = "Enable sending VPC flow logs into CloudWatch"
  default     = false
}
