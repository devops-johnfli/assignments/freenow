data "aws_availability_zones" "available" {
  exclude_names = var.vpc_excluded_azs
}
