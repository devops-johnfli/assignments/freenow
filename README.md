# FreeNow Tech Challenge

🙏 Welcome to this repo FreeNow team 🎉

This is **John Flionis**, honestly pleased to meet you, and thank you for hopping on the solution of this exciting challenge with me!

This repo attempts to provide the IaC for the AWS and k8s resources towards solving the tasks of FreeNow's [Cloud Engineer](https://boards.greenhouse.io/freenow/jobs/5397157) position challenge, also known as Case Study, link to pdf [here](https://drive.google.com/file/d/1uL6KTDmMS471x6PLva62EV2nFhrRGyva/view?usp=sharing).

## Progress

- [x] Develop IaC modules
- [x] Put all components together in the [freenow](terraform/environments/freenow/) environment
- [x] Debug code
- [x] Test
- [x] Design architecture diagram 
- [x] Add documentation


## Documentation

The code for provisioning, configuration and management of the infrastructure components has been developed in a modularized way to reduce compexity and maximize reusability of components.

The high-level view of the repo is the following:

```bash
terraform/
├── environments
│   ├── freenow
│   └── test
└── modules
    ├── dns
    ├── networking
    ├── rds
    ├── s3
    └── website
```

Where the TF modules are defined inside the [modules](terraform/modules/) directory and can then be reused - with custom configuraiton for each environment's needs - as defined in the [environments](terraform/environments/) directory. Here we have two environments:
- [freenow](terraform/environments/freenow/), and 
- [test](terraform/environments/test/),

we will be focusing on the first one for the rest of the present document. 

For more information on the specifications of the TF modules defined in the [modules](terraform/modules/) directory, you can navigate to the specific subdirectory of each. In the corresponding `variables.tf` file, every configurable variable of the module per se is followed by a complementary description and, in most cases, an indicative default value, as well.

## FreeNow Environment

The [freenow](terraform/environments/freenow/) environment, defines and puts together all the AWS resources required towards offering a solution to the [FreeNow Case Study](https://drive.google.com/file/d/1uL6KTDmMS471x6PLva62EV2nFhrRGyva/view?usp=sharing).

### Architecture Diagram

An overview of the architecture is provided in the architecture diagram below:

![Architecture Diagram](resources/freenow-architecture-diagram.drawio.png)

The structure of the [freenow](terraform/environments/freenow/) environment directory is the following:

```bash
freenow
├── dns.tf
├── networking.tf
├── providers.tf
├── rds.tf
├── s3.tf
├── variables.tf
├── versions.tf
└── website.tf
```

Let's discuss upon the TF files of the [freenow](terraform/environments/freenow/) environment one by one in order to understand a little bit better what AWS resources the code provisions and why:
- [dns.tf](terraform/environments/freenow/dns.tf)
  - where the [dns](terraform/modules/dns/) module is used to: 
    - create a **Route53 hosted zone** for the website
    - create an **ACM certificate**
    - add the corresponding **records** for DNS validation of the afore-created certificate
    - **validate** the certificate
  - **Assumptions**: Regarding DNS, we have made the assumption that either an existing domain has already been purchased or registered via Amazon Route53, or somebody has to add the name servers from the Route53 Hosted Zone in the domain registrar per se for DNS delegation to Route53. A helpful guide to achieve this for a CloudFlare domain can be found in this resource: 
    - [Configuring DNS Delegation from CloudFlare to AWS Route53 | vEducate blog article by Dean Lewis](https://veducate.co.uk/dns-delegation-route53/)
- [networking.tf](terraform/environments/freenow/networking.tf)
  - where the [networking](terraform/modules/networking/) module is used, which in turn uses the official community-supported [vpc](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest) module under-the-hood to:
    - create the **VPC**, alongside:
      - **public subnets** to host the Application Load Balancer (ALB) to accept the incoming traffic for the website
      - **private subnets** to host the EC2 instances and ECS cluster components to actually host the website
      - **database subnets** to host the RDS database for the website
      - **routing tables** for all subnets above
      - **internet gateway** for the public subnets to communicate to the internet, in order to accept incoming traffic
      - **NAT gateway** for the resources on the private subnets to be able to access the internet, in order to enable critical tasks like getting images from public registries
    - enable collecting [VPC flow logs](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html) into [CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html) or S3 bucket, if configured to do so
    - create **VPC endpoints**, in order to route requests to the corresponding AWS services internally within our VPC, instead of routing them over the internet, which incurs additional costs for traffic leaving our VPC, as well as raises security and latency considerations as well. The AWS services the module provisions VPC endpoints for are:
      - **S3**
      - **ECS**
      - **RDS**
- [providers.tf](terraform/environments/freenow/providers.tf)
  - where the required [TF providers](https://developer.hashicorp.com/terraform/language/providers) are initialized in order to be passed to the modules that will be using them to provision resources
- [rds.tf](terraform/environments/freenow/rds.tf)
  - where the [rds](terraform/modules/rds/) module is used, which in turn uses the official community-supported [rds-aurora](https://registry.terraform.io/modules/terraform-aws-modules/rds-aurora/aws/latest) module under-the-hood to:
    - create an **RDS Aurora cluster** to ensure high-availability, scalability, and data redundancy for the RDS database
    - **encrypt data at-rest** by [enabling storage encyption](terraform/modules/rds/postgresql.tf?plain=1#L27) via KMS key
    - **encrypt data in-transit** by [forcing SSL connections](terraform/modules/rds/postgresql.tf?plain=1#L55-L59)
    - **enable** enhanced **monitoring** and **logging** configuration options, if needed, like sending logs in [CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html), monitoring via [Enhanced Monitoring](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_Monitoring.OS.overview.html), or exporting [cluster activity stream](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/DBActivityStreams.Overview.html)
    - create 3 **backup plans** for the RDS data upon best practices:
      - one **daily**, where once backup is kept for each of the last 7 days
        - in order to obtain a recovery point for each day data loss or security breach, up to the level of a week
        - for daily backup only, we enable the continuous backup functionality to capture all changes in data and transaction logs for Amazon RDS and enable point-in-time restore (PITR), as described [here](https://docs.aws.amazon.com/aws-backup/latest/devguide/point-in-time-recovery.html)
      - one **weekly**, where once backup is kept for each of the last 3 weeks
        - in order to obtain recovery points for mid-term data loss or security breach, up to the level of a month
      - one **monthly**, where one backup is kept for each of the last 6 months,
        - in order to obtain recovery points for long-term data loss or security breach that has long gone undetected 
        - *Note: All but for the most recent month's backups are getting archived for cost-efficiency*
  - **Assumptions**: For the RDS database we made the assumption that it holds critical user/customer data, which are structured, need to be saved in a reliable and secure medium, and to be frequently accessed via complex queries that might occasionally combine them to extract valuable information. For the RDS database, a plain Amazon RDS could have been used, however, Amazon Aurora RDS offers increased resilience and availability, as it's constantly holding and syncing 2 copies of the database data volumes, across 3 AZs, as well as increased performance, 3-times more read replicas, PITR feature. For a more in-depth dive into the differences between the two refer to:
    - [Aurora Vs. RDS: Choosing The Best AWS Database Solution | Cloudzero blog article by Cody Slingerland](https://www.cloudzero.com/blog/aurora-vs-rds/)
- [s3.tf](terraform/environments/freenow/s3.tf)
  - where the [rds](terraform/modules/rds/) module is used, which in turn uses the official community-supported [rds-aurora](https://registry.terraform.io/modules/terraform-aws-modules/rds-aurora/aws/latest) module under-the-hood to:
    - create the **S3 bucket** for the website
    - create and put in effect a **bucket policy** to allow all operations for everyone in the principle of the AWS account per se
    - use a KMS key to **encrypt** the bucket **content**
    - conditionaly create an **expiration policy** for the bucket objects, if needed, based on configuration variables
    - conditionaly enable **bucket versioning**, if neede, based on configuration variables
  - **Assumptions**: For the S3 bucket we made the assumption that it holds critical user/customer data, which are not structured in any way, need to be persisted in a reliable and secure medium, like files, images, videos or documents. Replication of the S3 bucket data was considered to not bring any value, for the purposes of the demo.
- [variables.tf](terraform/environments/freenow/variables.tf)
  - where the most important configuration parameters for the environment can be set accordingly to one's needs
  - the variables are followed by a description and a default value
  - for more details refer to [variables.tf](terraform/environments/freenow/variables.tf)
- [versions.tf](terraform/environments/freenow/versions.tf)
  - where the version compatibility details per required TF provider are defined for the environemt
- [website.tf](terraform/environments/freenow/website.tf)
  - where the infrastructure to actually host the website is defined combining:
    - an [ALB](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html) to acccept the incoming traffic in a scalable way, as well as, serving for TLS termnation, using the ACM certificate created in [dns.tf](terraform/environments/freenow/dns.tf)
    - an [Autoscaling Group (ASG)](https://docs.aws.amazon.com/autoscaling/ec2/userguide/auto-scaling-groups.html) to scale more infrastructure components, i.e. EC2 instances, as needed based on demand/traffic
    - an [ECS cluster](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html) to run ECS services
    - an [ECS service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs_services.html) to deploy, keep constantly running, and autoscale ECS tasks for the actual hosting of the website; in the format of a container running on EC2 instances powered by the afore-mentioned ASG
  - **Assumptions**: To begin with, we're making the assumption that the website code is written following a **stateless** software architecture, making it eligible for **horizontal autoscaling**. In terms of traffic we're making the assumption that the simple website will have **minimal traffic** to begin with, yet it should be **able to scale automatically**, i.e. without any intervention from an engineer, in order to always be as responsive as possible, following user-demand needs, possibly bringing fluctuations of the incoming traffic. At the same time, the architectural decision had to make sense from a **cost-management** perspective, as ECS brings no additional costs, but for the actual AWS infrastructure running the code, more info on the official [ECS pricing page](https://aws.amazon.com/ecs/pricing/). Provisioning and managing a whole kubernetes cluster to orchestrate and autoscale the mere containers that host a website would add an inexplicable cost and engineering overhead. Deploying EC2 VMs under an Autoscaling Group, would also make sense, however, leveraging the capabilities of ECS simplifies tasks like **monitoring**, **logging**, **deployemnt of new code releases**, etc, while also being **more future-proof**; in case our website needs to be complemented by other services in the future, as those can be easily appended as extra containers into our stack, running on the same infrastructure while staying isolated and being autoscaled, as well.

<details><summary><b>Resources</b></summary>

1. [Configuring DNS Delegation from CloudFlare to AWS Route53 | vEducate blog article by Dean Lewis](https://veducate.co.uk/dns-delegation-route53/)
2. [How to Build AWS VPC using Terraform – Step by Step | Spacelift blog article by Flavius Dinu, Sumeet Ninawe](https://spacelift.io/blog/terraform-aws-vpc)
3. [How to Deploy an AWS ECS Cluster with Terraform \[Tutorial\] | Spacelift blog article by Sumeet Ninawe](https://spacelift.io/blog/terraform-ecs)
4. [Deploying Containers on Amazon’s ECS using Fargate and Terraform: Part 1 | Medium article by Bradford Lamson-Scribner](https://medium.com/@bradford_hamilton/deploying-containers-on-amazons-ecs-using-fargate-and-terraform-part-1-a5ab1f79cb21)
5. [Deploying Containers on Amazon’s ECS using Fargate and Terraform: Part 2 | Medium article by Bradford Lamson-Scribner](https://medium.com/@bradford_hamilton/deploying-containers-on-amazons-ecs-using-fargate-and-terraform-part-2-2e6f6a3a957f)
6. [ECS with Terraform PoC | GitHub Gist by nicosingh](https://gist.github.com/nicosingh/8b06190b8d675779617a8045b44f0582)


</details>

